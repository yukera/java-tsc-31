package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final Optional<Project> project = projectRepository.findById(userId, projectId);
        if (!project.isPresent()) throw new AccessDeniedException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task bindTaskByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final Optional<Task> task = taskRepository.findById(userId, taskId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        @NotNull final Optional<Project> project = projectRepository.findById(userId, projectId);
        if (!project.isPresent()) throw new AccessDeniedException();
        task.ifPresent(e -> e.setProjectId(projectId));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task unbindTaskByProjectId(@Nullable final String userId, @Nullable final String taskId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final Optional<Task> task = taskRepository.findById(userId, taskId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setProjectId(null));
        return task.get();
    }

    @Override
    @SneakyThrows
    public void clearProjects(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        for (@NotNull final Project project : projects) {
            @NotNull String projectId = project.getId();
            taskRepository.removeAllByProjectId(projectId);
        }
        projectRepository.clear();
    }

}
