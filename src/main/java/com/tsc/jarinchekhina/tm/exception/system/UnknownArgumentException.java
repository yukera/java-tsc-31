package com.tsc.jarinchekhina.tm.exception.system;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@NotNull final String arg) {
        super("Error! Argument '" + arg + "' not supported...");
    }

}
