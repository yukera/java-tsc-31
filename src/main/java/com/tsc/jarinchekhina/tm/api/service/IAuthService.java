package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role... roles);

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
