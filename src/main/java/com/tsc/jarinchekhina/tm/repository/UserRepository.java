package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.entity.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.function.Predicate;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private Predicate<User> predicateByLogin(@NotNull final String login) {
        return e -> login.equals(e.getLogin());
    }

    @NotNull
    private Predicate<User> predicateByEmail(@NotNull final String email) {
        return e -> email.equals(e.getEmail());
    }

    @NotNull
    @Override
    public Optional<User> findByLogin(@NotNull final String login) {
        return entities.stream()
                .filter(predicateByLogin(login))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<User> findByEmail(@NotNull final String email) {
        return entities.stream()
                .filter(predicateByEmail(email))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<User> removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = findByLogin(login);
        user.ifPresent(e -> entities.remove(e));
        return user;
    }

}
