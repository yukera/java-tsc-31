package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "finish task by index";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().finishTaskByIndex(userId, index);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER};
    }

}
