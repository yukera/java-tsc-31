package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "remove project by index";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Optional<Project> projectByIndex = serviceLocator.getProjectService().findByIndex(userId, index);
        if (!projectByIndex.isPresent()) throw new ProjectNotFoundException();
        @NotNull final Optional<Project> project = serviceLocator.getProjectTaskService().removeProjectById(userId, projectByIndex.get().getId());
        if (!project.isPresent()) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER};
    }

}
