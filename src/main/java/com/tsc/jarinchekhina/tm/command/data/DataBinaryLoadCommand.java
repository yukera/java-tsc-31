package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import com.tsc.jarinchekhina.tm.dto.Domain;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.system.NoFileException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-binary-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load binary data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BINARY DATA]");
        @NotNull final File file = new File(FILE_BINARY);
        if (!file.exists()) throw new NoFileException(FILE_BINARY);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
