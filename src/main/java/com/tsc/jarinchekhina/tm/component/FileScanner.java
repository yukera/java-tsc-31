package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void init() {
        if (DataUtil.isEmpty(commands)) commands.addAll(bootstrap.getCommandService().getCommandNames());
        @NotNull final Long delay = bootstrap.getPropertyService().getBackupInterval();
        executorService.scheduleWithFixedDelay(this,0, delay, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File("./");
        @Nullable final File[] files = file.listFiles();
        if (files == null) return;
        for (@NotNull final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            bootstrap.parseCommand(fileName);
            item.delete();
        }
    }

}
